Feature: Test API with Rest Assured

  Scenario: The URL will be returning all sports.
    Given Create a GET request to thesportsdb.com with endpoint "/all_sports.php"
    Then Status code response is 200 and Will displayed sport name:
      | sport name |
      | Soccer     |
      | Motorsport |
      | Fighting   |
      | Baseball   |
      | Basketball |

  Scenario: The URL will be return all leagues
    Given Create a GET request to the website thesportsdb.com with endpoint "/all_leagues.php"
    Then Status code response is 200 and will display 933 leagues include:
      | leagues |
      | Premier League |
      | Championship |
      | Scottish Premiership |
      | Bundesliga |

  Scenario: The URL will be return all Leagues in a country
     Given Create a GET request to the website thesportsdb.com with endpoint all Leagues in England "/search_all_leagues.php?c=England"
     Then Status code response is 200 and will display 33 leagues
     And The result have must strCountry tag with value is "England" and the result have must tags include:
       | tags |
       | idLeague |
       | strSport |
       | strLeague |
       | dateFirstEvent |
       | strCountry |

  Scenario: The URL will be return all teams in a League
    Given Create a GET request to the website thesportsdb.com with endpoint  "/lookupplayer.php?id=34145937"
    Then Status code response is 200 and will idPlayer is "34145937"

  Scenario:The URL will be return All Leagues
    Given Create a GET  request to the website thesportsdb.com with endpoint "/all_leagues.php"
    Then Status code response is 200
    And The isLeague should be displayed



