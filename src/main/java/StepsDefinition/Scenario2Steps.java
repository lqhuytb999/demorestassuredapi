package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import model.BaseAPIMethod;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario2Steps {

    BaseAPIMethod apiMethod = new BaseAPIMethod();

    @Given("Create a GET request to the website thesportsdb.com with endpoint {string}")
    public void createAGETRequestToTheWebsiteThesportsdbComWithEndpoint(String endPoint) {
        apiMethod.setUpRequest();
        apiMethod.methodGET(endPoint);
    }

    @Then("Status code response is {int} and will display {int} leagues include:")
    public void statusCodeResponseIsAndWillDisplayedLeagues(int code,int numberOfLeagues,DataTable dataTable) {
        apiMethod.verifyStatusCode(code);
        List<String> jsonData = apiMethod.jsonData("$..strLeagueAlternate");
        System.out.println(jsonData);
        Assert.assertEquals(jsonData.size(),numberOfLeagues);
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);

        for (int i=0;i<dataTable.height()-1;i++){
            String value = data.get(i).get("leagues");
            Assert.assertTrue(jsonData.toString().contains(value));
        }
    }

}
