package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import model.BaseAPIMethod;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario3Steps {

    BaseAPIMethod apiMethod = new BaseAPIMethod();

    @Given("Create a GET request to the website thesportsdb.com with endpoint all Leagues in England {string}")
    public void createAGETRequestToTheWebsiteThesportsdbComWithEndpointAllLeaguesInEngland(String endPoint) {
        apiMethod.setUpRequest();
        apiMethod.methodGET(endPoint);
    }

    @Then("Status code response is {int} and will display {int} leagues")
    public void statusCodeResponseIsAndWillDisplayedLeagues(int code,int numberOfLeagues) {
        apiMethod.verifyStatusCode(code);
        List<String> jsonData = apiMethod.jsonData("$..strLeagueAlternate");
        Assert.assertEquals(jsonData.size(),numberOfLeagues);
    }

    @And("The result have must strCountry tag with value is {string} and the result have must tags include:")
    public void theResultHaveMustStrCountryTagWithValueIsEnglandAndTheResultHaveMustTagsInclude(String country,DataTable dataTable) {
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        List<String> jsonData = apiMethod.jsonData("$..strCountry");
        Assert.assertTrue(jsonData.contains(country));
        List<String> jsonDataCountry = apiMethod.jsonData("$.countries");
        System.out.println(jsonDataCountry);
        for (int i=0;i<dataTable.height()-1;i++){
            String value = data.get(i).get("tags");
            Assert.assertTrue(jsonDataCountry.toString().contains(value));
        }
    }

}
