package StepsDefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import model.BaseAPIMethod;

import static org.hamcrest.Matchers.containsString;

public class Scenario5Steps {
    BaseAPIMethod apiMethod = new BaseAPIMethod();

    @Given("Create a GET  request to the website thesportsdb.com with endpoint {string}")
    public void createAGETRequestToTheWebsiteThesportsdbComWithEndpoint(String endPoint) {
        apiMethod.setUpRequest();
        apiMethod.methodGET(endPoint);
    }

    @Then("Status code response is {int}")
    public void statusCodeResponseIs(int code) {
        apiMethod.verifyStatusCode(code);
    }

    @And("The isLeague should be displayed")
    public void theIsLeagueShouldBeDisplayed() {
        Response res = apiMethod.getResponse();
        res.then().body(containsString("idLeague"));
    }
}
