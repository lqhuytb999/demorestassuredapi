package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import model.BaseAPIMethod;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario1Steps {

    BaseAPIMethod apiMethod = new BaseAPIMethod();

    @Given("Create a GET request to thesportsdb.com with endpoint {string}")
    public void createAGETRequestToThesportsdbComWithEndpointIs(String endpoint) {
        apiMethod.setUpRequest();
        apiMethod.methodGET(endpoint);
    }

    @Then("Status code response is {int} and Will displayed sport name:")
    public void verifyStatusCodeAndItem(int code ,DataTable dataTable) {
        apiMethod.getResponseBody();
        apiMethod.verifyStatusCode(code);
        List<String> jsonData = apiMethod.jsonData("$..strSport");
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        for (int i=0;i<dataTable.height()-1;i++){
            String value = data.get(i).get("sport name");
            Assert.assertTrue(jsonData.toString().contains(value));
        }
    }


}
