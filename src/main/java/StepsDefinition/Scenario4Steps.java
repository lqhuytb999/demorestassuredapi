package StepsDefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import model.BaseAPIMethod;
import org.testng.Assert;

import java.util.List;


import static io.restassured.path.json.JsonPath.from;

public class Scenario4Steps {
    BaseAPIMethod apiMethod = new BaseAPIMethod();

    @Given("Create a GET request to the website thesportsdb.com with endpoint  {string}")
    public void createAGETRequestToTheWebsiteThesportsdbComWithEndpoint(String endPoint) {
        apiMethod.setUpRequest();
        apiMethod.methodGET(endPoint);
    }

    @Then("Status code response is {int} and will idPlayer is {string}")
    public void statusCodeResponseIsAndWillIdPlayerIs(int code, String id) {
        apiMethod.verifyStatusCode(code);
        List<String> jsonData = apiMethod.jsonData("$..idPlayer");
        Assert.assertEquals(id,jsonData.get(0));
    }
}
