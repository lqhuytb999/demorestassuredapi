package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;



@CucumberOptions(
            features = "src/main/resources/TestAPI.feature",
            glue = {"StepsDefinition"},
            plugin = {"pretty", "html:report.html", "com.epam.reportportal.cucumber.ScenarioReporter"}
)
public class TestRunner extends AbstractTestNGCucumberTests {
}
